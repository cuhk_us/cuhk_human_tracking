#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from nus_human_tracking.msg import Poi

def PoiNode():
    pub = rospy.Publisher('/gcs/poi', Poi, queue_size=1)
    rospy.init_node('poi_node')
    while not rospy.is_shutdown():
        poi1 = Poi()
        poi1.x = 340
        poi1.y = 200
        pub.publish(poi1)
        rospy.sleep(1.0)

if __name__ == '__main__':
    try:
        PoiNode()
    except rospy.ROSInterruptException:
        pass
