#!/usr/bin/env python
import cv2
from cv_bridge import CvBridge, CvBridgeError
import rospy
from sensor_msgs.msg import Image
from nus_human_tracking.msg import Rect, InitTracker
from darknet_ros_msgs.msg import BoundingBoxes, BoundingBox
import message_filters

class ClipHumanNode(object):
    def __init__(self):
        super(ClipHumanNode, self).__init__()
        rospy.init_node('crop_human', anonymous=True)
        self.state_pub = rospy.Publisher('/gcs/init_tracker', InitTracker, queue_size=1)
        self.sub_image = message_filters.Subscriber("/darknet_ros/detection_image", Image)
        self.sub_detection = message_filters.Subscriber("/darknet_ros/bounding_boxes", BoundingBoxes)
        self.ts = message_filters.ApproximateTimeSynchronizer([self.sub_image, self.sub_detection], 10, 0.3)
        self.ts.registerCallback(self.img_callback)
        
        self.detect_boxes = BoundingBoxes()
        self.init_tracker = InitTracker()
        self.start_flag = False
        self.point = []
        rospy.spin()

    def select_point(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDBLCLK:
            cv2.circle(self.image, (x, y), 3, (255, 0, 0), -1)
            self.point.append(x)
            self.point.append(y)

    def img_callback(self, img_data, boxes):
        if not self.start_flag:
            try:
                # Convert image to numpy array
                self.image = CvBridge().imgmsg_to_cv2(img_data, "bgr8")

            except CvBridgeError as e:
                rospy.logerr(e)
            self.detect_boxes = boxes

            clone = self.image.copy()
            cv2.namedWindow("image")
            cv2.setMouseCallback("image", self.select_point)

            while (1):
                cv2.imshow('image', self.image)
                for i, box in enumerate(self.detect_boxes.bounding_boxes):
                    cv2.rectangle(self.image, (box.xmin, box.ymin), (box.xmax, box.ymax), (0, 255, 0), 3)
                k = cv2.waitKey(20) & 0xFF
                if k == 27:
                    break
            cv2.destroyAllWindows()

            if len(self.point) == 0:
                return
            print ("Selected Coordinates: %d, %d" % (self.point[0], self.point[1]))

            flag = False
            cx = 0
            cy = 0
            width = 0
            height = 0
            dist_to_poi = 100000
            for i, box in enumerate(self.detect_boxes.bounding_boxes):
                if self.inbox_check(box.xmin, box.ymin, box.xmax, box.ymax, self.point[0], self.point[1]):
                    current_dist = self.dist_calculate(box.xmin, box.ymin, box.xmax, box.ymax, self.point[0], self.point[1])
                    if current_dist < dist_to_poi:
                        dist_to_poi = current_dist
                        cx, cy, width, height = self.minmax_to_cxywh(box.xmin, box.ymin, box.xmax, box.ymax)
                        flag = True
            if flag:
                rospy.loginfo("Start tracking" )
                self.init_tracker.cx = cx
                self.init_tracker.cy = cy
                self.init_tracker.width = width
                self.init_tracker.height = height
                self.init_tracker.img = CvBridge().cv2_to_imgmsg(clone, "bgr8")
                self.state_pub.publish(self.init_tracker)
            self.start_flag = True

    def inbox_check(self, xmin, ymin, xmax, ymax, xpoi, ypoi):
        if xpoi > xmin and ypoi > ymin and xpoi < xmax and ypoi < ymax:
            return True
        else:
            return False

    def dist_calculate(self, xmin, ymin, xmax, ymax, xpoi, ypoi):
        return max(abs(xpoi - (xmin + xmax)/2), abs(ypoi - (ymin + ymax)/2))

    def minmax_to_cxywh(self, xmin, ymin, xmax, ymax):
        cx = (xmin + xmax)/2
        cy = (ymin + ymax)/2
        width = xmax - xmin
        height = ymax - ymin
        return cx, cy, width, height

def main():
    node = ClipHumanNode()

if __name__ == '__main__':
    main()

