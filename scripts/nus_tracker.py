#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from nus_human_tracking.msg import Rect, HumanPosition, Poi, InitTracker
from sensor_msgs.msg import Image
from darknet_ros_msgs.msg import BoundingBoxes, BoundingBox
import message_filters
import tf
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import TransformStamped, Vector3Stamped, PointStamped, Point

import cv2
from cv_bridge import CvBridge, CvBridgeError
import glob, torch
import numpy as np
from os.path import realpath, dirname, join

from net import SiamRPNvot
from run_SiamRPN import SiamRPN_init, SiamRPN_track
from utils import cxy_wh_2_rect
import time
import math

debug = ''

class HumanTrackerNode(object):
    def __init__(self):
        super(HumanTrackerNode, self).__init__()
        rospy.init_node('human_tracker', anonymous=True)
        self.pub_tracker = rospy.Publisher('/human_tracker/track_result', Point, queue_size=1)  # rospy.Publisher('/dummy/pos_cmd', Point, queue_size=1)
        self.pub_track_image = rospy.Publisher('/human_tracker/track_image', Image, queue_size=1)
        # self.sub_poi = rospy.Subscriber("/gcs/poi", Poi, self.poi_callback)
        self.sub_init = rospy.Subscriber('/gcs/init_tracker', InitTracker, self.init_callback)
        if debug == 'zed':
            self.sub_image = message_filters.Subscriber("/zed/left/image_rect_color", Image)
            self.sub_depth = message_filters.Subscriber("/zed/depth/depth_registered", Image)
            self.focal_length = 350
            self.xc_camera = 337.432
            self.yc_camera = 181.365
            self.human_height = 1.5
            self.human_width = 0.5
            self.score_thresh = 0.9
            self.w_patch = 20
            self.h_patch = 40
            self.bound_thresh = 5
            self.use_bound_thresh = 0
            self.safe_dist = 5
            self.safe_dist2 = 3
            self.togo_ratio = 0.9
        elif debug == 'simu':
            self.sub_image = message_filters.Subscriber("/hil/sensor/stereo/left/image_raw", Image)
            self.sub_depth = message_filters.Subscriber("/sensor/image", Image)
            self.focal_length = 415.7
            self.xc_camera = 320
            self.yc_camera = 240
            self.human_height = 1.5
            self.human_width = 0.5
            self.score_thresh = 0.9
            self.w_patch = 20
            self.h_patch = 40
            self.bound_thresh = 5
            self.use_bound_thresh = 0
            self.safe_dist = 5
            self.safe_dist2 = 3
            self.togo_ratio = 0.9
        else:
            self.sub_image = message_filters.Subscriber("/camera/image", Image)
            self.sub_depth = message_filters.Subscriber("/camera/depth", Image)
            (self.score_thresh, self.w_patch, self.h_patch) = self.get_parameters()
            (self.xc_camera, self.yc_camera, self.focal_length, self.human_height, self.human_width,
            self.bound_thresh, self.use_bound_thresh, self.safe_dist, self.safe_dist2, self.togo_ratio) = self.get_settings()
        self.ts = message_filters.ApproximateTimeSynchronizer([self.sub_image, self.sub_depth], 10, 0.3)
        self.ts.registerCallback(self.img_callback)
        self._bridge = CvBridge()
        self.point_in_optical = PointStamped()
        self.point_in_map = PointStamped()
        self.UAV_position = PointStamped()
        self.self_position = PointStamped()
        self.point_togo = Point()
        self.tracked_result = HumanPosition()
        self.listener = tf.TransformListener()
        # self.detect_boxes = BoundingBoxes()
        # self.current_image = np.ndarray()
        self.start_flag = False
        self.init_tracker = InitTracker()
        # load net
        self.net = SiamRPNvot()
        self.net.load_state_dict(torch.load(join(realpath(dirname(__file__)), 'SiamRPNVOT.model')))
        self.net.eval().cuda()
        rospy.spin()

    # def detect_callback(self, det_boxes):
    #     self.detect_boxes = det_boxes

    def inbox_check(self, xmin, ymin, xmax, ymax, xpoi, ypoi):
        if xpoi > xmin and ypoi > ymin and xpoi < xmax and ypoi < ymax:
            return True
        else:
            return False

    def dist_calculate(self, xmin, ymin, xmax, ymax, xpoi, ypoi):
        return max(abs(xpoi - (xmin + xmax)/2), abs(ypoi - (ymin + ymax)/2))

    def minmax_to_cxywh(self, xmin, ymin, xmax, ymax):
        cx = (xmin + xmax)/2
        cy = (ymin + ymax)/2
        width = xmax - xmin
        height = ymax - ymin
        return cx, cy, width, height

    def human_dist(self, track_res, imheight):
        # if track_res[3] / track_res[2] > 1.2 * self.human_height / self.human_width:
        #     dist_to_human = self.focal_length * self.human_height / track_res[3]
        # elif track_res[3] / track_res[2] < 0.8 * self.human_height / self.human_width:
        #     dist_to_human = self.focal_length * self.human_width / track_res[2]
        # else:
        #     dist_to_human = self.focal_length *(self.human_height / track_res[3] + self.human_width / track_res[2])
        if track_res[1] > self.bound_thresh and track_res[1] + track_res[3] / 2 < imheight - self.bound_thresh:
            dist_to_human = self.focal_length * self.human_height / track_res[3]
        else:
            dist_to_human = self.focal_length * self.human_width / track_res[2]
        return dist_to_human

    def init_callback(self, data):
        if not self.start_flag:
            rospy.loginfo("Start tracking" )
            # image and init box
            self.init_tracker = data
            cx = int(self.init_tracker.cx)
            cy = int(self.init_tracker.cy)
            w = int(self.init_tracker.width)
            h = int(self.init_tracker.height)
            # tracker init
            target_pos, target_sz = np.array([cx, cy]), np.array([w, h])
            image = self._bridge.imgmsg_to_cv2(self.init_tracker.img, "bgr8")
            self.state = SiamRPN_init(image, target_pos, target_sz, self.net)
            self.start_flag = True

    def img_callback(self, img_data, depth_data):
        try:
            self.current_image = self._bridge.imgmsg_to_cv2(img_data, "bgr8")
            self.depth_img = self._bridge.imgmsg_to_cv2(depth_data,desired_encoding='32FC1')
        except CvBridgeError as e:
            rospy.logerr(e)

        if self.start_flag:
            # tracking and visualization
            toc = 0
            tic = cv2.getTickCount()
            tic = time.time()
            self.state = SiamRPN_track(self.state, self.current_image)  # track
            success = self.state['score'] > self.score_thresh
            rospy.loginfo('tracking score: %f' % self.state['score'])
            res = cxy_wh_2_rect(self.state['target_pos'],
                                self.state['target_sz'])  # convert [cx,cy,w,h] to [minx,miny, w,h]
            imheight = self.current_image.shape[0]
            imwidth = self.current_image.shape[1]
            if self.use_bound_thresh:
                if (res[0] < self.bound_thresh or res[0] + res[2] > imwidth - self.bound_thresh
                        or res[1] < self.bound_thresh or res[1] + res[3] > imheight - self.bound_thresh):
                    success = False
            res = [int(l) for l in res]

            toc += cv2.getTickCount() - tic
            toc = time.time() - tic
            rospy.loginfo('Tracking core algorithm time %f s' % (toc))

            if success:
                depth_array = np.array(self.depth_img, dtype=np.float32)
                x_target = int(self.state['target_pos'][0])
                y_target = int(self.state['target_pos'][1])
                w_target = int(self.state['target_sz'][0])
                h_target = int(self.state['target_sz'][1])
                if h_target > self.h_patch:
                    h_target = self.h_patch
                if w_target > self.w_patch:
                    w_target = self.w_patch

                z_patch = depth_array[y_target-h_target/2 : y_target+h_target/2, x_target-w_target/2 : x_target+w_target/2]
                z = np.median(z_patch[~np.isnan(z_patch)])
                x = z * (x_target - self.xc_camera) / self.focal_length
                y = z * (y_target - self.yc_camera) / self.focal_length
                toc = time.time() - tic
                rospy.loginfo('After distance estimation %f s' % (toc))
                # print("human position: %f, %f, %f" % (x, y, z))

                # self.depthimg = cv2.normalize(depth_array, depth_array, 0, 1, cv2.NORM_MINMAX)
                # cv2.imshow("Depth", self.depthimg)
                # cv2.waitKey(1)

                # dist_to_human = self.human_dist(res, self.current_image.shape[0])
                # self.tracked_result.distance = dist_to_human
                self.tracked_result.success = success
                self.tracked_result.cx = x_target
                self.tracked_result.cy = y_target
                self.tracked_result.width = w_target
                self.tracked_result.height = h_target

                self.self_position.point.x = 0
                self.self_position.point.y = 0
                self.self_position.point.z = 0

                if debug == 'simu':
                    self.point_in_optical.point.x = z + 0.3
                    self.point_in_optical.point.y = -x + 0.1
                    self.point_in_optical.point.z = -y
                else:
                    self.point_in_optical.point.x = x
                    self.point_in_optical.point.y = y
                    self.point_in_optical.point.z = z
                if debug == 'zed':
                    (self.point_in_optical.header.frame_id) = "/zed_left_camera_optical_frame"
                    self.self_position.header.frame_id = "/zed_camera_center"
                    self.point_in_map = self.listener.transformPoint("/map", self.point_in_optical)
                elif debug == 'simu':
                    (self.point_in_optical.header.frame_id) = "/hil/state/measurement"
                    self.self_position.header.frame_id = "/hil/state/measurement"
                    # (self.point_in_optical.header.frame_id) = "/hil/left_camera_optical_frame"
                    self.point_in_map = self.listener.transformPoint("/map", self.point_in_optical)
                else:
                    self.point_in_optical.header.frame_id, map_id, self.self_position.header.frame_id = self.get_frame_names()
                    self.point_in_map = self.listener.transformPoint(map_id, self.point_in_optical)
                    self.UAV_position = self.listener.transformPoint(map_id, self.self_position)

                rospy.loginfo('Human position in image: cx=%d, cy=%d, w=%d, h=%d' % (res[0], res[1], res[2], res[3]))
                rospy.loginfo('Human position in camera coordinate: x=%f, y=%f, z=%f' % (x, y, z))
                rospy.loginfo('Human position in map coordinate: x=%f, y=%f, z=%f' % (self.point_in_map.point.x, self.point_in_map.point.y, self.point_in_map.point.z))
                toc = time.time() - tic
                rospy.loginfo('After transform point %f s' % (toc))

                dx = self.point_in_map.point.x - self.UAV_position.point.x
                dy = self.point_in_map.point.y - self.UAV_position.point.y
                dz = self.point_in_map.point.z - self.UAV_position.point.z
                target_to_uav = math.sqrt(dx * dx + dy * dy + dz * dz)
                if target_to_uav > self.safe_dist:
                    ratio = self.safe_dist/target_to_uav
                elif target_to_uav >= self.safe_dist2:
                    ratio = 0.9
                else:
                    ratio = 1

                self.point_togo.x = self.point_in_map.point.x * (1 - ratio) + self.UAV_position.point.x * ratio
                self.point_togo.y = self.point_in_map.point.y * (1 - ratio) + self.UAV_position.point.y * ratio
                self.point_togo.z = self.point_in_map.point.z * (1 - ratio) + self.UAV_position.point.z * ratio
                toc = time.time() - tic
                rospy.loginfo('After target calculation %f s' % (toc))
                self.pub_tracker.publish(self.point_togo)
                cv2.rectangle(self.current_image, (res[0], res[1]), (res[0] + res[2], res[1] + res[3]), (0, 255, 255), 3)
            else:
                rospy.loginfo('tracking lost')

            try:
                self.pub_track_image.publish(self._bridge.cv2_to_imgmsg(self.current_image, "bgr8"))
            except CvBridgeError as e:
                rospy.logerr(e)
            toc = time.time() - tic
            rospy.loginfo('All tracking process time %f s' % (toc))

    def get_parameters(self):
        """
        Args:
        Returns:
        (tuple) (score_thresh, w_patch, h_patch)
        """
        score_thresh = rospy.get_param('~score_thresh')
        w_patch = rospy.get_param('~w_patch')
        h_patch = rospy.get_param('~h_patch')
        return (score_thresh, w_patch, h_patch)

    def get_settings(self):
        """
        Args:
        Returns:
        (tuple) (xc_camera, yc_camera, focal_length, human_height, human_width)
        """
        xc_camera = rospy.get_param('~xc_camera')
        yc_camera = rospy.get_param('~yc_camera')
        focal_length = rospy.get_param('~focal_length')
        human_height = rospy.get_param('~human_height')
        human_width = rospy.get_param('~human_width')
        bound_thresh = rospy.get_param('~bound_thresh')
        use_bound_thresh = rospy.get_param('~use_bound_thresh')
        safe_dist = rospy.get_param('~safe_dist')
        safe_dist2 = rospy.get_param('~safe_dist2')
        togo_ratio = rospy.get_param('~togo_ratio')
        return (xc_camera, yc_camera, focal_length, human_height, human_width, bound_thresh, use_bound_thresh, safe_dist, safe_dist2, togo_ratio)

    def get_frame_names(self):
        """
        Args:
        Returns:
        (tuple) (optical_frame_id, global_frame_id)heighta
        """
        optical_frame_id = rospy.get_param('~optical_frame_id')
        global_frame_id = rospy.get_param('~global_frame_id')
        body_frame_id = rospy.get_param('~body_frame_id')
        return (optical_frame_id, global_frame_id, body_frame_id)

def main():
    """ main function
    """
    node = HumanTrackerNode()

if __name__ == '__main__':
    main()

