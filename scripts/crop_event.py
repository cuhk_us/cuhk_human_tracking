#!/usr/bin/env python
import argparse
import cv2
from cv_bridge import CvBridge, CvBridgeError
import rospy
from sensor_msgs.msg import Image
from nus_human_tracking.msg import Rect, InitState
 
# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not

class CropHumanNode(object):
    def __init__(self):
        super(CropHumanNode, self).__init__()
        rospy.init_node('crop_human', anonymous=True)
        self.state_pub = rospy.Publisher('/gcs/init_state', InitState, queue_size=1)
        self.sub_image = rospy.Subscriber("/camera/image", Image, self.img_callback, queue_size=1, buff_size=1)
        self.init_state = InitState()
        self._bridge = CvBridge()
        self.start_flag = False
        rospy.spin()

    def img_callback(self, data):
        if not self.start_flag:
            try:
                # Convert image to numpy array
                self.image = self._bridge.imgmsg_to_cv2(data, "bgr8")

            except CvBridgeError as e:
                rospy.logerr(e)
            # grab references to the global variables
            global refPt, cropping
            refPt = []
            cropping = False

            def click_and_crop(event, x, y, flags, param):
                global refPt, cropping
            
                # if the left mouse button was clicked, record the starting
                # (x, y) coordinates and indicate that cropping is being
                # performed
                if event == cv2.EVENT_LBUTTONDOWN:
                    refPt = [(x, y)]
                    cropping = True
            
                # check to see if the left mouse button was released
                elif event == cv2.EVENT_LBUTTONUP:
                    # record the ending (x, y) coordinates and indicate that
                    # the cropping operation is finished
                    refPt.append((x, y))
                    cropping = False
            
                    # draw a rectangle around the region of interest
                    cv2.rectangle(self.image, refPt[0], refPt[1], (0, 255, 0), 2)
                    cv2.imshow("image", self.image)

            clone = self.image.copy()
            cv2.namedWindow("image")
            cv2.setMouseCallback("image", click_and_crop)

            # keep looping until the 'q' key is pressed
            while True:
                # display the image and wait for a keypress
                cv2.imshow("image", self.image)
                key = cv2.waitKey(1) & 0xFF

                # if the 'r' key is pressed, reset the cropping region
                if key == ord("r"):
                    image = clone.copy()

                # if the 'c' key is pressed, break from the loop
                elif key == ord("c"):
                    break

            # if there are two reference points, then crop the region of interest
            # from teh image and display it
            if len(refPt) == 2:
                roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
                # print("box: xmin=%d, xmax=%d, ymin=%d, ymax=%d" % (refPt[0][0], refPt[1][0], refPt[0][1], refPt[1][1]))
                cx = (refPt[0][0]+refPt[1][0])/2
                cy = (refPt[0][1]+refPt[1][1])/2
                w = refPt[1][0]-refPt[0][0]
                h = refPt[1][1]-refPt[0][1]
                print("box: cx=%d, cy=%d, w=%d, h=%d" % (cx, cy, w, h) )
                cv2.imshow("ROI", roi)
                cv2.waitKey(0)

            # close all open windows
            cv2.destroyAllWindows()

            self.init_state.cx = cx
            self.init_state.cy = cy
            self.init_state.width = w
            self.init_state.height = h
            self.init_state.img = self._bridge.cv2_to_imgmsg(self.image, "bgr8")
            self.state_pub.publish(self.init_state)

def main():
    node = CropHumanNode()

if __name__ == '__main__':
    main()
