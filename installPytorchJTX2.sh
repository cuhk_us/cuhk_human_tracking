#!/bin/sh

cd

cp ~/.bashrc ~/backup_bashrc.txt

echo "export CUDNN_LIB_DIR=/usr/lib/aarch64-linux-gnu" >> ~/.bashrc

echo "export CUDNN_INCLUDE_DIR=/usr/include" >> ~/.bashrc

source ~/.bashrc

sudo apt-get update -y

sudo apt-get -y install cmake

sudo apt-get -y install libffi-dev libopenblas-dev libatlas-dev liblapack-dev liblapacke-dev checkinstall

sudo apt-get -y install python-pip

sudo pip install numpy scipy

sudo pip install pyyaml scikit-build cffi typing future

cd pytorch

sudo nvpmodel -m 0

sudo ~/jetson_clocks.sh

sudo python setup.py install

cd

sudo pip --no-cache-dir install torchvision==0.2.1

sudo pip install pandas

sudo pip install Cython

sudo pip install 'networkx==2.2'

sudo pip install 'scikit-image<0.15'
