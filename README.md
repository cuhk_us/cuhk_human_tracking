# Prerequisites

ros kinetic

python2.7

pytorch == 0.4.1

torchvision == 0.2.1

numpy

opencv

cuda == 9.0

# Install on PC

sudo apt-get -y install cmake

sudo apt install libffi-dev libopenblas-dev libatlas-dev liblapack-dev liblapacke-dev checkinstall

sudo apt-get install python-pip

pip install numpy scipy pyyaml scikit-build cffi opencv-python

pip install torch==0.4.1

pip install torchvision==0.2.1

# Install on TX2

sudo gedit ~/.bashrc

export CUDNN_LIB_DIR=/usr/lib/aarch64-linux-gnu

export CUDNN_INCLUDE_DIR=/usr/include

source ~/.bashrc

sudo apt-get -y install cmake

sudo apt install libffi-dev libopenblas-dev libatlas-dev liblapack-dev liblapacke-dev checkinstall

sudo apt-get install python-pip

sudo pip install numpy scipy

sudo pip install pyyaml scikit-build cffi typing future

git clone http://github.com/pytorch/pytorch

git checkout -b v0.4.1 v0.4.1

cd pytorch

git submodule update --init --recursive

sudo nvpmodel -m 0

sudo  ~/jetson_clocks.sh

sudo python setup.py install

sudo pip --no-cache-dir install torchvision==0.2.1

sudo pip install pandas

sudo pip install Cython

sudo pip install scikit-image

# Step 1
roslaunch darknet_ros darknet_ros_zed.launch

roslaunch nus_human_tracking track_zed.launch

roslaunch nus_human_tracking click_event.launch

# Step 2

double click within the bounding box in the image, press the key "esc"

# view result without rviz
rosrun image_view image_view image:=/darknet_ros/detection_image
rosrun image_view image_view image:=/human_tracker/track_image