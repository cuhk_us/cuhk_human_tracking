#!/bin/sh
cd
sudo apt-get install openjdk-8-jdk
wget https://download-cf.jetbrains.com/python/pycharm-community-2019.1.tar.gz
tar -zxvf pycharm-community-2019.1.tar.gz
sudo ln -s /home/nvidia/pycharm-community-2019.1/bin/pycharm.sh /usr/bin/pycharm